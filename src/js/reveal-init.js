import Reveal from '../../node_modules/reveal.js/dist/reveal.esm.js';
import Markdown from '../../node_modules/reveal.js/plugin/markdown/markdown.esm.js';
import Highlight from '../../node_modules/reveal.js/plugin/highlight/highlight.esm.js';
import Notes from '../../node_modules/reveal.js/plugin/notes/notes.esm.js';

Reveal.initialize({ 
  history: true,
  width: 1600,
  height: 1000,
  margin: 0.04,
  center: false,
  slideNumber: true,
  mouseWheel: true,
  
  // Learn about plugins: https://revealjs.com/plugins/
  plugins: [ Markdown, Highlight, Notes ]
});
