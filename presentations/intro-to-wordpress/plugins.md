# Plugins

**Plugins** are additional packages that you can install to extend the functionality of WordPress

*   Thousands of free plugins available on WordPress.org
*   Can be downloaded/installed from the WordPress admin
*   Some popular plugins:
    *   **Akismet** - antispam protection for comments
    *   **Yoast SEO** - search engine optimization