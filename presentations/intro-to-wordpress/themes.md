# Themes

**Themes** define the look and feel of your website

- Comprised of templates and styles that define layout, colors, typography, etc.
- Thousands of free themes available on WordPress.org
- Can be downloaded/installed from the WordPress admin
- WordPress comes with a default theme, named after the year:
  - Twenty Seventeen, Twenty Sixteen, etc.
- Numerous commercial theme vendors
