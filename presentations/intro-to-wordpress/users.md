# Users

**Users** refers to registered accounts on your website

*   Best practise => one User per person
*   Users have a _Role_ assigned that defines permissions for what they can do on your site 
    *   **Super Admin** – full administratoraccess to a multi-site network \*
    *   **Administrator**: full access to all admin features
    *   **Editor**: can publish and manage posts of all users
    *   **Author**: can publish and manage only their own posts
    *   **Contributor**: can write and manage their own posts but cannot publish them
    *   **Subscriber**: can only manage their user profile

\* only available across a multi-site [Network](https://codex.wordpress.org/Create_A_Network)