// webpack.mix.js

let mix = require('laravel-mix');

mix.setPublicPath('dist');

mix.sass('node_modules/reveal.js/css/reveal.scss', 'css')
.sass('src/scss/rexrana-light.scss', 'css')
.sass('src/scss/rexrana-dark.scss', 'css')
.sass('src/scss/unbranded-light.scss', 'css')
.sass('src/scss/unbranded-dark.scss', 'css')
.sass('src/scss/bootstrap.scss', 'css')
.options({
    processCssUrls: false
});

mix.js('src/js/reveal-init.js', 'js');

mix.copy('src/css', 'dist/css');

// mix.browserSync({
//     server: true,
//     injectChanges: false,
// });