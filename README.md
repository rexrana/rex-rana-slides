# Rex Rana presentation slides

Presentations given by Rex Rana, and our templates for the [reveal.js](https://revealjs.com/) framework.

Feel free to fork the presentations and use as you wish. If you republish, please give credit to Rex Rana Design and Development Ltd.

## Templates

If you are using these templates for your own presentations, and are not an employee or contractor of Rex Rana, please use the unbranded versions.

- [Dark](templates/unbranded/dark.html)
- [Light](templates/unbranded/light.html)

## Installation

First, check out the repository or download archive and extract.

From the root folder, run `npm install` to install the project dependencies

## Usage

To serve locally, first install the dependencies. 

We use [nvm](https://github.com/nvm-sh/nvm) to manage node versions, and have included a `.nvmrc` file. If you are using nvm, you can match the node version used for development by running:

```bash
nvm install
nvm use
```

This will install the matching node version mentioned in the `.nvmrc` file. Then simply install the dependencies using npm.

```bash
npm install
```

## Serving

To serve these presentations, you can:

* place this whole folder in a public directory on a web server, or: 
* run a local development server via BrowserSync and Laravel Mix, by running:

```bash
npm run watch
```

## License

[GNU General Public License, Version 3](LICENSE)

Copyright (C) 2016-2024 [Rex Rana Design and Development Ltd.](https://rexrana.ca)
